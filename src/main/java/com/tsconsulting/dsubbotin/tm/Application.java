package com.tsconsulting.dsubbotin.tm;

import com.tsconsulting.dsubbotin.tm.constant.ArgumentConst;
import com.tsconsulting.dsubbotin.tm.constant.TerminalConst;
import com.tsconsulting.dsubbotin.tm.model.Command;
import com.tsconsulting.dsubbotin.tm.repository.CommandRepository;
import com.tsconsulting.dsubbotin.tm.util.NumberUtil;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        displayWelcome();
        parseArgs(args);
        process();
    }

    private static void parseArgs(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        exit();
    }

    private static void parseArg(final String arg) {
        switch (arg) {
            case ArgumentConst.CMD_ABOUT: displayAbout(); break;
            case ArgumentConst.CMD_HELP: displayHelp(); break;
            case ArgumentConst.CMD_VERSION: displayVersion(); break;
            case ArgumentConst.CMD_INFO: displayInfo(); break;
            default: displayArgError(); break;
        }
    }

    private static void process() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!TerminalConst.CMD_EXIT.equals(command)) {
            command = scanner.nextLine();
            parseCommand(command);
        }
    }

    private static void parseCommand(final String command) {
        switch (command) {
            case TerminalConst.CMD_ABOUT: displayAbout(); break;
            case TerminalConst.CMD_HELP: displayHelp(); break;
            case TerminalConst.CMD_VERSION: displayVersion(); break;
            case TerminalConst.CMD_INFO: displayInfo(); break;
            case TerminalConst.CMD_EXIT: exit(); break;
            default: displayCommandError(); break;
        }
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void displayAbout() {
        System.out.println("Developer: Dmitriy Subbotin");
        System.out.println("E-Mail: dsubbotin@tsconsulting.com");
    }

    private static void displayVersion() {
        System.out.println("1.7.3");
    }

    private static void displayInfo() {
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

    private static void displayHelp() {
        Command[] commands = CommandRepository.getCommands();
        for (final Command command : commands) {
            System.out.println(command);
        }
    }

    private static void exit() {
        System.exit(0);
    }

    private static void displayArgError() {
        System.err.println("Argument not found!");
        System.exit(1);
    }

    private static void displayCommandError() {
        System.err.println("Command not found!");
    }

}
