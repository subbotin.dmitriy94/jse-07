package com.tsconsulting.dsubbotin.tm.constant;

public final class ArgumentConst {

    public static final String CMD_ABOUT = "-a";

    public static final String CMD_VERSION = "-v";

    public static final String CMD_INFO = "-i";

    public static final String CMD_HELP = "-h";

}
