package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.constant.ArgumentConst;
import com.tsconsulting.dsubbotin.tm.constant.TerminalConst;
import com.tsconsulting.dsubbotin.tm.model.Command;

public class CommandRepository {

    private static final Command ABOUT = new Command(TerminalConst.CMD_ABOUT, ArgumentConst.CMD_ABOUT, "Display developer info.");

    private static final Command VERSION = new Command(TerminalConst.CMD_VERSION, ArgumentConst.CMD_VERSION, "Display program version.");

    private static final Command INFO = new Command(TerminalConst.CMD_INFO, ArgumentConst.CMD_INFO, "Display system info.");

    private static final Command HELP = new Command(TerminalConst.CMD_HELP, ArgumentConst.CMD_HELP, "Display list of terminal commands.");

    private static final Command EXIT = new Command(TerminalConst.CMD_EXIT, null, "Exit application.");

    private static final Command[] COMMANDS = new Command[]{
            ABOUT, VERSION, INFO, HELP, EXIT
    };

    public static Command[] getCommands() {
        return COMMANDS;
    }
}
